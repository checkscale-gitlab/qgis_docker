**NOTE:** This is a rough, personal and in progress guide to make a QGIS image using Docker. For the moment it is only in Spanish but you could follow the source that I guess it is easy to follow. 

**Contenidos:**
<!-- TOC -->
- [Ubuntu + repositorios QGIS](#ubuntu--repositorios-qgis)
- [Dockerfile](#dockerfile)
- [Imágenes listas para usar](#imágenes-listas-para-usar)
- [Corriendo QGIS vía `ssh -X` (en pruebas)](#corriendo-qgis-vía-ssh--x-en-pruebas)
- [Notas temporales sobre en uso de `supervisor`](#notas-temporales-sobre-en-uso-de-supervisor)
- [Para mantener perfiles y opciones de manera persistente](#para-mantener-perfiles-y-opciones-de-manera-persistente)
<!-- /TOC -->

# Licencia

El siguiente código está bajo licencia [Creative Commons Reconocimiento - Compartir igual 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

20210322 - __CC BY-SA 4.0__ - _Miguel Sevilla-Callejo_

- - -

# Archivos para correr QGIS desde Docker

Este repositorio contiene las sentencias y los archivos para construir imágenes de Docker que tiene como propósito realizar pruebas personales con las últimas versiones de QGIS en la rama de desarrollo, última .

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/QGIS_logo%2C_2017.svg/320px-QGIS_logo%2C_2017.svg.png)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Docker_%28container_engine%29_logo.svg/320px-Docker_%28container_engine%29_logo.svg.png)


__Este código está en proceso de mejora y cualquier aportación es bienvenida.__

__Atención__: las imágenes resultantes __tienen un gran tamaño__ (pueden pasar de 3.1 GB).

Si quieres una imagen de QGIS generada desde la compilación del código del programa te recomiendo que le eches un vistazo a las imágenes oficiales del proyecto QGIS en [Docker Hub](https://hub.docker.com/r/qgis/qgis/) o en el [repo oficial de GitHub](https://github.com/qgis/QGIS/tree/master/.docker). 

# Debian/Ubuntu + repositorios QGIS

Originalmente se se trabajó cono sobre una imagen de [Ubuntu LTS (focal/ 20.04)](https://hub.docker.com/_/ubuntu), luego se ha reemplazado por [Debian 10 (buster)](https://hub.docker.com/_/ubuntu) a las que se les han añadido los repositorios de QGIS y se les han incluido, por un lado, y siguiendo la image nde docker original del proyecto, `supervisor` o, por otro lado el servicio SSH a través del cual, podremos correr QGIS en el _host_.

Aquí he listado los comandos incluidos en el contenedor de pruebas original (para la versión _Latest Release_):

```bash
# instalar paquetes necesarios
apt-get install -y wget gpg ssh nano

# en la imagen original de ubuntu o debian se necesita un ambiente de escritorio, Desktop Environment.
# por defecto se instala gnome-shell que es muy pesado por lo que se ha cambiado a uno más ligero.
apt-get install -y lxqt-core

# importar clave de repo QGIS
apt-key adv --keyserver keyserver.ubuntu.com --recv-key 51F523511C7028C3

# añadir repo de QGIS
echo -e "# QGIS Latest Release\ndeb http://qgis.org/ubuntu focal main" \
    > /etc/apt/sources.list.d/qgis-latest.list

# para debian buster la línea del repositorio es:
# deb http://qgis.org/debian buster main

# instalar QGIS
apt install qgis qgis-plugin-grass

# configurar servidor ssh para que corran las X fuera del contenedor
# configurar sshd_config con `X11UseLocalhost no`
sed -i 's/\#X11UseLocalhost yes/X11UseLocalhost no/g' /etc/ssh/sshd_config

# arrancar ssh (esto no funciona en el container, desactivado por defecto)
service ssh start

# crear usuario qgis
    # esto no funciona, la contraseña ha de estar cifrada
#useradd -u 12345 -g users -d /home/username -s /bin/bash -p '12345' username

    # es mejor usar `useradd` en vez de `adduser` que es interactivo para ñadir usuario. 
useradd --create-home --shell /bin/bash qgis

    # añadir contraseña de usuario -- NO FUNCIONA
echo -n 'qgis:qgis2020' | chpasswd

# crear directorio de datos del usuario qgis
mkdir -p /home/qgis/data
chown 777 /home/qgis/data

# instalar xvfb y supervisor
sudo apt-get -y install xvfb supervisor
```

NOTA: Las últimas versiones de los archivos `dockerfile` se han realizado instalando `supervisor`, que nos permite poder correr QGIS en las X del host copiando el método que hay en el _dockerfile_ oficial, tal y como se describe a continuación. Para más información sobre supervisor se puede revisar: [docker-cookbooks/Dockerfile at master · Krijger/docker-cookbooks · GitHub](https://github.com/Krijger/docker-cookbooks/blob/master/supervisor/Dockerfile)

<!-- Pendiente de REVISAR el docker de supervidor para ver que hace exactamente -->


# Dockerfile

En este repositorio se han incluido los tres archivos del tipo `dockerfile` que recoger parte de los comandos que están más arriba.

- [qgis_dev_buster.dockerfile](qgis_dev_buster.dockerfile) - para versión de desarrollo
- [qgis_lr_buster.dockerfile](qgis_lr_buster.dockerfile) - para versión de largo recorrido
- [qgis_ltr_buster.dockerfile](qgis_ltr_buster.dockerfile) - para última versión estable

A partir de cualquiera de los anterior archivos se puede construir una imagen Docker para ejecutar QGIS (en cualquiera de las tres versiones), para ello se usa el comando `docker build` sobre directorio de trabajo del siguiente modo (ejemplo para la versión de desarrollo):

```bash
# versión de desarrollo; QGIS 3.19 (master)
docker build -t qgis:dev -f qgis_dev_buster.dockerfile .
```

# Imágenes listas para usar

A 21 de marzo de 2021 se han subido las imágenes correspondientes al uso de estos archivos `dockerfile` a mi [repositorio personal en DockerHub](https://hub.docker.com/u/msevilla00) y se pueden lanzar desde allí con el comando `docker run` (a continuación ejemplo para la versión en desarrollo) precedido de la sentencia `xhost + &` que permite conectar con el servidor X:

```bash
# para versión en desarrollo; QGIS 3.19 (master)
xhost + &
docker run --rm -it --name qgis_dev \
   -v $PWD:/root \
   -v /tmp/.X11-unix:/tmp/.X11-unix \
   -e DISPLAY=unix$DISPLAY \
   msevilla00/qgis:dev qgis
```
Ten presente que el contenedor resultante de esa sentencia correrá sobre un directorio desde el que se ejecute, por la opción `$PWD`, pero esto puede cambiarse por ejemplo, por un directorio de datos de tu carpeta de usuario: `/home/user/data`


# Corriendo QGIS vía `ssh -X` (en pruebas)

Con el sistema anterior todo funciona de manera correcta pero es posible que en algún entorno de desarrollo (podría ser en macOS o Windows, aunque no he probado) pueda ser buena idea usar un acceso al docker por el sistema de SSH que indicaba más arriba. Para ello hay un archivo `dockerfile` en pruebas con esto.

El archivo en cuestión es [qgis_dev_ssh.dockerfile](qgis_dev_ssh.dockerfile)

Para correr container desde imagen, lanzar `docker run` en la carpeta a la que se desea tener acceso:

```bash
docker run --rm -ti \
    --name qgisdev \
    -v $PWD:/home/qgis/data \
    msevilla00/qgis_dev
```

Y una vez que se está corriendo el container, sobre la consola se puede arrancar ssh, ver qué IP tenemos:

```bash
service ssh start
hostname -I
```

Y nos podemos conectarnos desde el host a través de ssh del siguiente modo:

`ssh qgis@172.17.0.2 -X` y le damos la contraseña `qgis2020` (o la que esté indicada en el dockerfile)


# Para mantener perfiles y opciones de manera persistente

<!-- REVISAR para ver si funciona con la última versión -->

Los archivos con los perfiles de la imagen oficial se guardan en `/root/.local/share/QGIS/QGIS3/profiles` del container por lo que podemos guardar la configuración si generamos un volumen en nuestro directorio.

Así mismo podemos incluir un volumen dirigido a los datos sobre los que trabajar pero debemos tener en cuenta que __los enlaces NO se verán__ en el contenedor

Con lo anterior quedaría el siguiente comando para la versión en desarrollo:

```bash
xhost +
docker run --rm -it --name qgis_dev \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v ~/.docker_volumes/qgisdev:/root/.local/share/QGIS/QGIS3/profiles \
    -v $PWD:/data \
    -e DISPLAY=unix$DISPLAY \
    msevilla00/qgis:dev qgis
```
El directorio `~/.docker_volumes/qgisdev` sería aquel en el que quedarían los perfiles creados por ese contenedor.


# Corriendo QGIS vía `ssh -X`

Con el sistema anterior todo funciona de manera correcta pero es posible que en algún entorno de desarrollo (podría ser en macOS o Windows, aunque no he probado) pueda ser buena idea usar un acceso al docker por el sistema de SSH que indicaba más arriba. Para ello hay un archivo `dockerfile` en pruebas con esto.

El archivo en cuestión es [qgis_dev_ssh.dockerfile](qgis_dev_ssh.dockerfile)

Para correr container desde imagen, lanzar `docker run` en la carpeta a la que se desea tener acceso:

```bash
docker run --rm -ti \
    --name qgisdev \
    -v $PWD:/home/qgis/data \
    msevilla00/qgis_dev
```

Y una vez que se está corriendo el container, sobre la consola se puede arrancar ssh, ver qué IP tenemos:

```bash
service ssh start
hostname -I
```

Y nos podemos conectarnos desde el host a través de ssh del siguiente modo:

`ssh qgis@172.17.0.2 -X` (hay que revisar la IP interna de nuestro container, en este caso 172.17.0.2, pero puede ser otra) y le damos la contraseña `qgis2020` (o la que esté indicada en el dockerfile).


# Ejecutando las imágenes con Podman

El mismo sistema anterior puede ejecutarse con [Podman](https://podman.io/whatis.html), un sistema de contenedores compatible con Docker que permite correr esta arquitectura de virtualización ligera sin privilegios de administrador del sistema.

```bash
podman run --rm -it --name qgis_dev \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $PWD:/root \
    -e DISPLAY=unix$DISPLAY \
    msevilla00/qgis:dev qgis
```
Las pruebas que se han realizado han sido satisfactorias. Mientras que usando el sistema de Docker hasta ahora descrito los proyectos y los archivos se escriben como administrador del sistema, `root`.

Así mismo al lanzar el comando con `podman` se puede prescindir del comando previo `xhost +` pues se está ejecutando dentro de las variables de entorno del usuario.

Hay que tener presente que si se usa `podman` dentro del directorio desde el que se ha generado aparecerán varias carpetas ocultas relativas a los archivos de configuración de QGIS: `.cache .grass7 .local` que si están sobre el directorio de usuario entrarán en conflicto con la instalación de QGIS u otros contenedores que se lancen del mismo modo.

<!-- REVISAR esto:
https://dzone.com/articles/docker-without-root-privileges
 -->

# Ejemplo de archivo .desktop 

A continuación pego el código del archivo para crear el contenedor de QGIS vía Docker en GNU/Linux:

```ini
[Desktop Entry]
Type=Application
Icon=qgis
Name=Podman QGIS Dev
Terminal=false
Exec=sh -c 'podman run --rm --name qgis_dev -v /tmp/.X11-unix:/tmp/.X11-unix -v ~/GISData:/root -e DISPLAY=unix$DISPLAY msevilla00/qgis:dev qgis'
Categories=Qt;Education;Science;Geography;
MimeType=application/x-qgis-project;application/x-qgis-layer-settings;applicati$
Keywords=map;globe;postgis;wms;wfs;ogc;osgeo;
```

En el caso anterior lanza Podman en vez de Docker pero hay dos ejemplos de este tipo de archivos ligados a este repositorio: [qgis_latest_docker.desktop](qgis_latest_docker.desktop) y [qgis_latest_podman.desktop](qgis_latest_podman.desktop).


# Desarrollo futuro y otras opciones

Manejando las opciones de la sentencia del `docker run` se pueden incluir volúmenes que se conecten con una carpeta determinada para guardar la configuración de los perfiles de QGIS (opciones y plugins) o cambiar el directorio de trabajo.

Aquí he añadido las diferentes ideas u opciones para ir mejorando las imagenes Docker de base:

- [x] Cambiar instalación de gnome-shell (se instala por defecto) para aligerar imagen

- [x] Opcion a las tres versiones de QGIS, master, LR y LTR

- [x] Correr QGIS directamente en el host (vía xvfb + supervisor )

- [x] Añadir complementos de interés: GRASS, SAGA (en Dockerfile)

- [x] Probar imágenes con Podman

- [x] Incluir archivos `.desktop` de ejemplo en el repositorio 

- [ ] Probar con WMs ligeros como OpenBox o i3wm 

- [ ] Configurar adecuadamente volúmenes para salvar los perfiles y directorio de trabajo

- [ ] Revisar versión de GDAL (última versión?)


# Notas sobre la imagen oficial de QGIS (20200515)

Nota de cómo usar las imágenes oficiales de QGIS desde [sus notas en el repo de GitHub](https://github.com/qgis/QGIS/tree/master/.docker)

- última versión; de desarrollo; imagen 7 GB

```bash
xhost +
docker run --rm -it --name qgis_official \
    -v /tmp/.X11-unix:/tmp/.X11-unix  \
    -e DISPLAY=unix$DISPLAY \
    qgis/qgis:latest \
    qgis
```

Si queremos correr la versión 3.12 (imagen 5.6 GB) debemos usar la imagen `qgis/qgis:release-3_12_focal`


<!-- 

Pruebas no root

```bash
# pruebas con usuario id 1000
xhost + &
docker run --rm -it --name qgis_dev \
  -e PUID=1000 \
  -e PGID=1000 \
   -v $PWD:/root \
   -v /tmp/.X11-unix:/tmp/.X11-unix \
   -e DISPLAY=unix$DISPLAY \
   msevilla00/qgis:latest qgis
```
NO funciona! 

-->